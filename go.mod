module gitlab.com/staticvoidlabs/youtvservicesgo

go 1.15

require github.com/PuerkitoBio/goquery v1.7.1

require (
	github.com/gorilla/mux v1.8.0
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
)
