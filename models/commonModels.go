package models

// Section config models.
type Config struct {
	Version             string `json:"version"`
	ApiPort             int    `json:"apiPort"`
	YoutvUser           string `json:"youtvUser"`
	YoutvPass           string `json:"youtvPass"`
	DeleteAfterDownload string `json:"deleteAfterDownload"`
	DownloadPath        string `json:"downloadPath"`
}

// Section connection models.
type AuthenticityToken struct {
	Token string
}

type Project struct {
	Name string
}

// Section recording models.
type RecordingsRaw struct {
	Recs []RecordingRaw `json:"recordings"`
}

type RecordingRaw struct {
	Status string `json:"status"`
	ID     int    `json:"id"`
}

type RecordingsRepo struct {
	Recordings []RecordingDetails
	Queue      DownloadQueue
}

type Recording struct {
	RecDetails RecordingDetails `json:"recording"`
}

type RecordingDetails struct {
	ID               int            `json:"id"`
	Title            string         `json:"title"`
	SubTitle         string         `json:"subtitle"`
	DescShort        string         `json:"short_text"`
	DescLong         string         `json:"long_text"`
	Files            []DownloadFile `json:"files"`
	DownloadLink     string
	DateStarted      string `json:"starts_at"`
	DateEnded        string `json:"ends_at"`
	DateStartedClean string
	Duration         int `json:"duration"`
	Length           string
	Station          Channel `json:"channel"`
	IsNew            bool
}

type DownloadFile struct {
	File       string `json:"file"`
	Quality    string `json:"quality"`
	Resolution int    `json:"resolution"`
	Size       int    `json:"size"`
}

type Channel struct {
	Name string `json:"name"`
}

type DownloadJob struct {
	ID            int
	Filename      string
	DownloadURL   string
	State         int
	ShouldCancel  bool
	IsDownloading bool
}

type DownloadQueue struct {
	ActiveItemsCount int
	JobsInProgress   bool
	Jobs             []DownloadJob
}
