package main

import (
	Managers "gitlab.com/staticvoidlabs/youtvservicesgo/managers"
)

func main() {

	// Read and process config file.
	Managers.ProcessConfigFile()

	// Initialize YouTV subsystem and prepare repos.
	Managers.InitYoutvSubsystem()

	// Initailize GorillaMUX service and start listening...
	Managers.InitAPIService()

}
