package managers

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"strconv"

	"github.com/gorilla/mux"
)

// InitAPIService starts the API service.
func InitAPIService() {

	WriteLogMessage("Starting Web API services", true)

	// Create router instance.
	router := mux.NewRouter().StrictSlash(true)

	// Define routes and actions.
	router.HandleFunc("/", getStateInfoService)
	router.HandleFunc("/youtv", getStateInfo)
	router.HandleFunc("/youtv/update", updateRecordings)
	router.HandleFunc("/youtv/recordings", generateHTMLOutput)
	router.HandleFunc("/youtv/recordings/m", generateMobileHTMLOutput)
	router.HandleFunc("/youtv/recordings/raw", getRecordings)
	router.HandleFunc("/youtv/queue", showCurrentQueue)
	router.HandleFunc("/youtv/queue/add/{id}", addQueueItem)
	router.HandleFunc("/youtv/ctrl/restart", restartSubsystem)
	//router.PathPrefix("/nappy/news").Handler(http.StripPrefix("/nappy/news", http.FileServer(http.Dir("./www"))))

	tmpConfiguredApiPort := ":" + strconv.Itoa(mCurrentConfig.ApiPort)

	// Start rest service.
	go log.Fatal(http.ListenAndServe(tmpConfiguredApiPort, router))

}

// Endpoint implementation.
func getStateInfoService(w http.ResponseWriter, r *http.Request) {

	tmpLogMessages := GetCurrentLog()

	for _, message := range tmpLogMessages {
		w.Write([]byte(message + "\r\n"))
	}

}

func showCurrentQueue(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:showCurrentQueue", true)

	updateRepos()

	json.NewEncoder(w).Encode("VoidServices_response:show_current_queue:success.")
}

func updateRecordings(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:updateRecordings", true)

	updateRepos()

	json.NewEncoder(w).Encode("VoidServices_response:update_recordings:success.")

}

func getStateInfo(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:getStateInfo", true)

	json.NewEncoder(w).Encode("VoidServices_response:get_state_info:success.")
}

func getRecordings(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:getRecordings", true)

	//json.NewEncoder(w).Encode("VoidServices_response:get_recordings:success.")
	json.NewEncoder(w).Encode(mRecordingsRepo)
}

func restartSubsystem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:restartSubsystem", true)

	json.NewEncoder(w).Encode("VoidServices_response:restart_subsystem:success.")
}

func generateHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateHTMLOutput", true)

	// Try to parse template.
	tmpl, err := template.ParseFiles("./www/template_recordings_simple.html")
	if err != nil {
		fmt.Println("Error parsing template: ", err)
	}

	err = tmpl.Execute(w, mRecordingsRepo)
	if err != nil {
		fmt.Println("Error executing template: ", err)
	}

	//json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func addQueueItem(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:addQueueItem", true)

	params := mux.Vars(r)
	id, _ := params["id"]

	saveRecordingToDisk(id)

	fmt.Println("Added recording with id " + id + " to download queue.")

	json.NewEncoder(w).Encode("VoidServices_response:add_queue_item:success.")

}

func generateMobileHTMLOutput(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:generateMobileHTMLOutput", true)

	// Try to parse template.
	tmpl, err := template.ParseFiles("./www/template_recordings_simple.html")
	if err != nil {
		fmt.Println("Error parsing template: ", err)
	}

	err = tmpl.Execute(w, mRecordingsRepo)
	if err != nil {
		fmt.Println("Error executing template: ", err)
	}

	//json.NewEncoder(w).Encode("VoidServices_response:generate_HTML_Output:success.")
}

func htmlTesting(w http.ResponseWriter, r *http.Request) {

	WriteLogMessage("Request:testing", true)

	//json.NewEncoder(w).Encode("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
	fmt.Print("<p>Regards,<br/>Dark Universe Data Lake, managed by Crimson Macaw Wormhole</p>`")
}
