package managers

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/http/cookiejar"
	"net/url"
	"os"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/PuerkitoBio/goquery"

	Models "gitlab.com/staticvoidlabs/youtvservicesgo/models"
)

// ### Const und var definition. #################################################################
const (
	baseURL                   = "https://www.youtv.de"
	baseURLRecordingsListData = "https://www.youtv.de/api/v2/recs.json"
	baseURLRecordingData      = "https://www.youtv.de/api/v2/recordings"
)

var (
	mHttpCientInstance App
	mRecsRawData       Models.RecordingsRaw
	mRecordingsRepo    Models.RecordingsRepo
	//mDownloadQueue Models.DownloadQueue
)

// ### Local type definition. #################################################################
type App struct {
	Client *http.Client
}

// ### Private methods. #################################################################
func (app *App) getToken() Models.AuthenticityToken {
	loginURL := baseURL + "/login"
	client := app.Client

	response, err := client.Get(loginURL)

	if err != nil {
		log.Fatalln("Error fetching response. ", err)
	}

	defer response.Body.Close()

	document, err := goquery.NewDocumentFromReader(response.Body)
	if err != nil {
		log.Fatal("Error loading HTTP response body. ", err)
	}

	token, _ := document.Find("input[name='authenticity_token']").Attr("value")

	authenticityToken := Models.AuthenticityToken{
		Token: token,
	}

	return authenticityToken
}

func (app *App) login() {
	client := app.Client

	authenticityToken := app.getToken()

	loginURL := baseURL + "/login"

	data := url.Values{
		"authenticity_token": {authenticityToken.Token},
		"session[email]":     {mCurrentConfig.YoutvUser},
		"session[password]":  {mCurrentConfig.YoutvPass},
	}

	response, err := client.PostForm(loginURL, data)

	if err != nil {
		log.Fatalln(err)
	}

	defer response.Body.Close()

	_, err = ioutil.ReadAll(response.Body)
	if err != nil {
		log.Fatalln(err)
	}
}

func getRecordingsRawData() {

	response, err := mHttpCientInstance.Client.Get(baseURLRecordingsListData)

	if err != nil {
		log.Fatalln("Error fetching response. ", err)
	}

	defer response.Body.Close()

	body, readErr := ioutil.ReadAll(response.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	jsonErr := json.Unmarshal(body, &mRecsRawData)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

}

func getRecordingRawDataById(id int) Models.Recording {

	tmpURLRecordingDataToFetch := baseURLRecordingData + "/" + strconv.Itoa(id) + ".json"

	response, err := mHttpCientInstance.Client.Get(tmpURLRecordingDataToFetch)

	if err != nil {
		log.Fatalln("Error fetching response. ", err)
	}

	defer response.Body.Close()

	body, readErr := ioutil.ReadAll(response.Body)
	if readErr != nil {
		log.Fatal(readErr)
	}

	var tmpRecording Models.Recording

	jsonErr := json.Unmarshal(body, &tmpRecording)
	if jsonErr != nil {
		log.Fatal(jsonErr)
	}

	return tmpRecording
}

func prepareRecordingsRepo() {

	var tmpRecordingsRepo []Models.RecordingDetails

	for _, rec := range mRecsRawData.Recs {

		if rec.Status == "recorded" {

			tmpRecording := getRecordingRawDataById(rec.ID)

			var tmpRecordingClean Models.RecordingDetails
			tmpRecordingClean.ID = tmpRecording.RecDetails.ID
			tmpRecordingClean.Title = tmpRecording.RecDetails.Title
			tmpRecordingClean.SubTitle = tmpRecording.RecDetails.SubTitle
			tmpRecordingClean.DescShort = tmpRecording.RecDetails.DescShort
			tmpRecordingClean.DescLong = tmpRecording.RecDetails.DescLong
			tmpRecordingClean.Files = tmpRecording.RecDetails.Files
			tmpRecordingClean.DownloadLink = tmpRecording.RecDetails.Files[0].File
			tmpRecordingClean.DateStarted = tmpRecording.RecDetails.DateStarted
			tmpRecordingClean.DateStartedClean = getDateStartedCleanString(tmpRecordingClean.DateStarted)
			tmpRecordingClean.DateEnded = tmpRecording.RecDetails.DateEnded
			tmpRecordingClean.Duration = tmpRecording.RecDetails.Duration
			tmpRecordingClean.Length = getRecordingLengthString(tmpRecordingClean.Duration)
			tmpRecordingClean.Station = tmpRecording.RecDetails.Station
			tmpRecordingClean.IsNew = checkActuality(tmpRecordingClean.DateStarted)

			tmpRecordingsRepo = append(tmpRecordingsRepo, tmpRecordingClean)

		}
	}

	mRecordingsRepo.Recordings = tmpRecordingsRepo
}

func downloadFile(video Models.RecordingDetails) {

	resp, err := http.Get(video.DownloadLink)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Create the file
	out, err := os.Create("./test.mp4")
	if err != nil {
		fmt.Println(err)
	}
	defer out.Close()

	// Write the body to file
	_, err = io.Copy(out, resp.Body)
	fmt.Println(err)

	// Post processing: Name...

}

func getRecordingLengthString(duration int) string {

	tmpLengthString := ""

	if duration > 0 && duration < 120 {
		tmpLengthString = strconv.Itoa(duration) + "m"
	} else if duration >= 120 && duration < 180 {
		tmpLengthString = "2" + strconv.Itoa(duration-120) + "m"
	} else if duration >= 180 && duration < 240 {
		tmpLengthString = "3h" + strconv.Itoa(duration-180) + "m"
	} else if duration >= 240 && duration < 300 {
		tmpLengthString = "4h" + strconv.Itoa(duration-240) + "m"
	} else {
		tmpLengthString = strconv.Itoa(duration) + "m"
	}

	return tmpLengthString
}

func getDateStartedCleanString(dateStarted string) string {

	// 2021-09-24T10:30:00.000+02:00

	tmpDateString := ""

	tmpStringDate := dateStarted[0:10]
	tmpStringTime := dateStarted[11:16]

	tmpStringDate = strings.Replace(tmpStringDate, "-", "/", -1)

	tmpDateString = tmpStringDate + " " + tmpStringTime + " Uhr"

	return tmpDateString
}

func saveRecordingToDisk(recordingId string) {

	tmpRecordingID, _ := strconv.Atoi(recordingId)
	tmpCurrentRecordingObject := getRecordingObjectByID(tmpRecordingID)

	var tmpDownloadJob Models.DownloadJob
	tmpDownloadJob.ID = tmpRecordingID
	tmpDownloadJob.ShouldCancel = false
	tmpDownloadJob.State = 0
	tmpDownloadJob.DownloadURL = tmpCurrentRecordingObject.DownloadLink
	tmpDownloadJob.Filename = generateFilename(tmpCurrentRecordingObject.DownloadLink)

	fmt.Println("Starting download: " + strconv.Itoa(tmpCurrentRecordingObject.ID) + " " + tmpCurrentRecordingObject.Title)

	// Create the file
	updateDownloadQueueItemState(tmpCurrentRecordingObject.ID, 1)
	mRecordingsRepo.Queue.JobsInProgress = true
	out, err := os.Create(mCurrentConfig.DownloadPath + tmpDownloadJob.Filename)
	if err != nil {
		fmt.Println(err)
	}
	defer out.Close()

	// Get the data
	mRecordingsRepo.Queue.Jobs = append(mRecordingsRepo.Queue.Jobs, tmpDownloadJob)

	resp, err := http.Get(tmpDownloadJob.DownloadURL)
	if err != nil {
		fmt.Println(err)
	}
	defer resp.Body.Close()

	// Writer the body to file
	_, err = io.Copy(out, resp.Body)
	if err != nil {
		fmt.Println(err)
	}

	updateDownloadQueueItemState(tmpDownloadJob.ID, 2)
	fmt.Println("Finished download: " + strconv.Itoa(tmpCurrentRecordingObject.ID) + " " + tmpCurrentRecordingObject.Title)
}

func generateFilename(fullDownloadLink string) string {

	tmpFilename := ""

	tmpFilename = filepath.Base(fullDownloadLink)

	// Check if file looks like a Youtv file name.
	if len(tmpFilename) > 8 && len(strings.Split(tmpFilename[0:9], "-")) == 3 {

		tmpFileNamePartArray := strings.Split(tmpFilename, ".")
		tmpFileNameWithoutExt := tmpFileNamePartArray[0]

		// Parse broadcast date
		tmpDatePartsArray := strings.Split(tmpFileNameWithoutExt[:16], "_")
		tmpTimeStamp := strings.Replace(tmpDatePartsArray[0], "-", "", -1) + "_" + strings.Replace(tmpDatePartsArray[1], "-", "", -1)

		// Get short title and origin.
		tmpFileNameWithoutExt = tmpFileNameWithoutExt[17:len(tmpFileNameWithoutExt)]
		tmpArray2 := strings.Split(tmpFileNameWithoutExt, "_")
		tmpFileNameWithoutExt = tmpArray2[0]
		tmpFileNameWithoutExt = strings.Replace(tmpFileNameWithoutExt, "-", " ", -1)
		tmpStationName := tmpArray2[1]
		tmpFileNameOptimzed := "[YOUTV] " + tmpFileNameWithoutExt + " (" + tmpStationName + "_" + tmpTimeStamp + ")." + tmpFileNamePartArray[1]

		tmpFilename = tmpFileNameOptimzed
	}

	return tmpFilename
}

func updateDownloadQueueItemState(id int, state int) {

	for i, item := range mRecordingsRepo.Queue.Jobs {

		if item.ID == id {

			mRecordingsRepo.Queue.Jobs[i].State = state

			if state == 1 {
				mRecordingsRepo.Queue.Jobs[i].IsDownloading = true
			} else {
				mRecordingsRepo.Queue.Jobs[i].IsDownloading = false
			}

		}

	}

}

func IsDownloadInProgress() bool {

	for _, item := range mRecordingsRepo.Queue.Jobs {

		if item.State == 1 {
			mRecordingsRepo.Queue.JobsInProgress = true
			return true
		}

	}

	mRecordingsRepo.Queue.JobsInProgress = false

	return false
}

func getRecordingObjectByID(id int) Models.RecordingDetails {

	var tmpRecordingObject Models.RecordingDetails

	for _, rec := range mRecordingsRepo.Recordings {

		if rec.ID == id {
			tmpRecordingObject = rec
		}

	}

	return tmpRecordingObject
}

func getDownloadLinkByRecordID(id int) string {

	for _, rec := range mRecordingsRepo.Recordings {

		if rec.ID == id {
			return rec.DownloadLink
		}

	}

	return ""
}

func checkActuality(dateStarted string) bool {

	// 2021-09-24T10:30:00.000+02:00

	/*
		tmpRecordingIsNew := false

		tmpStringYear := strconv.Atoi((dateStarted[0:4])
		tmpStringMonth := strconv.Atoi(s)dateStarted[5:7])
		tmpStringDay := strconv.Atoi(s)dateStarted[8:10])
		tmpStringHour := strconv.Atoi(s)dateStarted[11:13])

		currentYear, currentMonth, currentDay := time.Now().Date()

		if currentYear == tmpStringYear && int(currentMonth) == tmpStringMonth && (currentDay-tmpStringDay == 1) {

		}

		fmt.Println(tmpStringYear + tmpStringMonth + tmpStringDay + tmpStringHour)

		return tmpRecordingIsNew
	*/

	return false
}

// ### Public methods. #################################################################
func InitYoutvSubsystem() {

	// 1. Establish network connection.
	jar, _ := cookiejar.New(nil)

	mHttpCientInstance = App{
		Client: &http.Client{Jar: jar},
	}

	mHttpCientInstance.login()

	// 2. Fill recordings repo.
	getRecordingsRawData()
	prepareRecordingsRepo()

}

func updateRepos() {
	getRecordingsRawData()
	prepareRecordingsRepo()
}

func DeleteRecording(id int) {

	client := mHttpCientInstance.Client

	tmpURLRecordingToDelete := baseURLRecordingData + "/" + strconv.Itoa(id) + ".json"

	// Create request
	req, err := http.NewRequest("DELETE", tmpURLRecordingToDelete, nil)
	if err != nil {
		fmt.Println(err)
		return
	}

	// Fetch Request
	resp, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
		return
	}
	defer resp.Body.Close()

	// Read Response Body
	_, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		fmt.Println(err)
		return
	}

	fmt.Println("response Status : ", resp.Status)
}
