package managers

import (
	"encoding/json"
	"fmt"
	"os"

	Models "gitlab.com/staticvoidlabs/youtvservicesgo/models"
)

var (
	mCurrentConfig Models.Config
)

// Private functions.
func ProcessConfigFile() {

	var currentConfig Models.Config

	configFile, err := os.Open("./config.json")
	defer configFile.Close()

	if err != nil {
		fmt.Println(err.Error())
	}

	jsonParser := json.NewDecoder(configFile)
	jsonParser.Decode(&currentConfig)

	mCurrentConfig = currentConfig
}
